use std::collections::HashMap;


//#1.1
pub fn is_unique(str: &str) -> bool {
    let str_ = str.as_bytes();
    for i in 0.. str_.len() {
        for j in 0.. str_.len(){
            if i != j && str_[i] == str_[j] {
                return false;
            }
        }
    }
    true
}

//#1.2
pub fn check_permutation(str1: &str, str2: &str) -> bool {
        if str1.len() == str2.len() {
            let mut chars1: HashMap<char, u32> = HashMap::new();
            let mut chars2: HashMap<char, u32> = HashMap::new();
            str1.to_string().chars().for_each(|x| if chars1.contains_key(&x) {
                                    if let Some(v) = chars1.get_mut(&x) {
                                        *v += 1;
                                    }
                                }
                                else{chars1.insert(x, 1);}
            );
            str2.to_string().chars().for_each(|x| if chars2.contains_key(&x) {
                if let Some(v) = chars2.get_mut(&x) {
                    *v += 1;
                }
            }
            else{chars2.insert(x, 1);}
            );
            for (k, v) in chars1.iter() {
                if let Some(v_) = chars2.get(k) {
                    if v == v_ {
                        continue;
                    }
                }
                return false;
            }
        }
        true
}