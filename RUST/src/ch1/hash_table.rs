// Hash Tables pg. 88

use std::hash::{Hash, Hasher};
use std::cmp::Eq;
use std::collections::hash_map::DefaultHasher;


const ARRAY_SIZE: usize = 30;
pub struct HashTable<H: Hash + Eq + Copy + Clone, G: Copy + Clone> {
    table: [Vec<Pair<H, G>>; ARRAY_SIZE],
}

#[derive(Copy, Clone)]
struct Pair<H: Hash + Eq + Copy, G: Copy + Clone>(H, G);


impl<H: Hash + Eq + Copy, G: Copy + Clone> HashTable<H, G> {
    pub fn new() -> Self {

        HashTable{table: Default::default()}
    }



    pub fn insert(&mut self, key: H, value: G){

        let hash = Self::calc_hash(key) as usize;
        let mut key_exists = false;

        for elem in &mut self.table[hash % ARRAY_SIZE] {
            if elem.0 == key {
                key_exists = true;
                elem.1 = value;
            }
        }
        if !key_exists {
            self.table[hash % ARRAY_SIZE].push(Pair(key, value));
        }
        
    }
    pub fn remove(&mut self, key: H) -> Option<G> {
        let hash = Self::calc_hash(key) as usize;
        for i in (0.. self.table[hash % ARRAY_SIZE].len()).rev() {
           if match self.table[hash % ARRAY_SIZE][i] {
                Pair(key_, _) => if key_ == key { true }
                                else {false},
            } {
                let v = self.table[hash % ARRAY_SIZE][i].1;
                self.table[hash % ARRAY_SIZE].swap_remove(i);
                return Some(v);
            }
        }
        None
    }

    pub fn get(&self, key: H) -> Option<G> {
        let hash = Self::calc_hash(key) as usize;
        for elem in &self.table[hash % ARRAY_SIZE] {
            if elem.0 == key {
                return Some(elem.1);
            }
        }
        None
    }

    pub fn count(&self) -> u64 {
        let mut count: u64 = 0;
        for elem in self.table.iter() {
            count += elem.len() as u64;
        }
        return count;
    }

    fn calc_hash(key: H) -> u64 {
        let mut hasher = DefaultHasher::new();
        key.hash(&mut hasher);
        hasher.finish()
    }
}

