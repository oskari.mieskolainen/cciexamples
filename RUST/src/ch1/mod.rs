pub mod hash_table;
mod tasks;

#[cfg(test)]
mod tests {
    use super::hash_table::HashTable;
    use super::tasks;

    #[test]
    fn test_hash_table(){
        let mut table: HashTable<&str, u32> = HashTable::new();
        table.insert("data", 5);

        assert_eq!(Some(5), table.get("data"));

        assert_eq!(1, table.count());

        table.remove("data");

        assert_eq!(0, table.count());
    }

    #[test]
    fn test_is_unique(){
        assert!(tasks::is_unique("abcde"));
        assert!(!tasks::is_unique("aabb"));
    }

    #[test]
    fn test_permutation(){
        unimplemented!();
    }
}
